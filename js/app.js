/* Función para generar números 
    aleatorios y almacenarlos en
    un arreglo */
function generarNumeros() {
    let arreglo = [];

    for(let indice = 0; indice < 20; indice++) {
        arreglo.push(Math.floor(Math.random() * (100 - 1) + 1)); 
    }

    rellenarCampos(arreglo);
}

/* Función que recibe un arreglo 
    de números enteros */
function rellenarCampos(arreglo) {
    let txtArreglo = document.getElementById('txtArreglo');
    let txtPromedio = document.getElementById('txtPromedio');
    let txtPares = document.getElementById('txtPares');
    let txtOrdenado = document.getElementById('txtOrdenado');

    txtArreglo.value = arreglo;
    txtPromedio.value = calcularPromedio(arreglo);
    txtPares.value = cantidadPares(arreglo);
    txtOrdenado.value = insertionSort(arreglo);
}

/* Función para retornar el promedio
    de los números almacenados en el
    arreglo */
function calcularPromedio(arreglo) {
    let promedio = 0;

    for(let indice = 0; indice < 20; indice++) {
        promedio+=arreglo[indice];
    }

    return promedio/20;
}

/* Función para retornar la cantidad
    de pares que se encuentran en el
    arreglo */
    function cantidadPares(arreglo) {
        let pares = 0;
    
        for(let indice = 0; indice < 20; indice++) {
            if(arreglo[indice] % 2 == 0) {
                pares++;
            }
        }
    
        return pares;
    }

/* Función que retorna el arreglo ordenado
    de manera descendiente */
function insertionSort(arreglo) {
    for(let j = 1; j < 20; j++) {
        let key = arreglo[j];
        let indice = j - 1;

        while(indice > -1 && arreglo[indice] < key) {
            arreglo[indice + 1] = arreglo[indice];
            indice = indice - 1;
        }

        arreglo[indice + 1] = key;
    }

    return arreglo;
}